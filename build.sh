echo "Azure functions deployment"

#clean up builds
echo "Clean up existing packages..."
rm -rf ./bin
mkdir bin

echo "Scenario1: Building node hop1 package"
cd ./functions/scenario1_hop1/
zip -r ../../bin/scenario1_hop1_node.zip *

cd ../scenario1_hop2
zip -r ../../bin/scenario1_hop2_node.zip *