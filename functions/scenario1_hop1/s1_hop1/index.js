module.exports = function (context, req) {
    context.log('JavaScript HTTP trigger function processed a request.');

    if (req.query.func_code) {
        
        var request = require("request"); 
        var post_data = JSON.stringify({ "name" : "Hello from S1_hop1 function"});
        var uri =  "https://scenario1-hop2-azure-function.azurewebsites.net/api/s1_hop2?code=" + req.query.func_code;
        context.log("Path to request: " + uri);

        request.post(uri,post_data,function (error, response, body) {
            if (!error) {
                context.log("Successfully");
                context.res = {
                    body: body
                };
                context.done(); 
            }
            else{
                context.log("Error occured");
                context.res = {
                    status: 400,
                    body: error
                };
                context.done(); 
            }
        });
    } else {
        context.res = {
            status: 400,
            body: "Please pass a func_code on the query string"
        };
        context.done(); 
    }
};