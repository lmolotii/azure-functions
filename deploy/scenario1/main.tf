# variables
variable "location" {}

variable "resource_group_name" {}
variable "app_service_plan_id" {}
variable "storage_connection_string" {}
variable "instrumentation_key" {}

variable "function_app_name" {
  default = "scenario1_hop1"
}

#resources

resource "azurerm_function_app" "azure_function_scenario1_hop1" {
  name                      = "scenario1-hop1-azure-function"
  location                  = "${var.location}"
  resource_group_name       = "${var.resource_group_name}"
  app_service_plan_id       = "${var.app_service_plan_id}"
  storage_connection_string = "${var.storage_connection_string}"

  app_settings {
    APPINSIGHTS_INSTRUMENTATIONKEY = "${var.instrumentation_key}"
    HASH                           = "${base64sha256(file("./../bin/scenario1_hop1_node.zip"))}"
    WEBSITE_USE_ZIP                = "https://github.com/lmolotii/azure-functions-playgroud/raw/master/scenario1_hop1_node.zip"
  }
}

resource "azurerm_function_app" "azure_function_scenario1_hop2" {
  name                      = "scenario1-hop2-azure-function"
  location                  = "${var.location}"
  resource_group_name       = "${var.resource_group_name}"
  app_service_plan_id       = "${var.app_service_plan_id}"
  storage_connection_string = "${var.storage_connection_string}"

  app_settings {
    APPINSIGHTS_INSTRUMENTATIONKEY = "${var.instrumentation_key}"
    HASH                           = "${base64sha256(file("./../bin/scenario1_hop2_node.zip"))}"
    WEBSITE_USE_ZIP                = "https://github.com/lmolotii/azure-functions-playgroud/raw/master/scenario1_hop2_node.zip"
  }
}
