provider "azurerm" {}

resource "azurerm_resource_group" "rg" {
  name     = "Azure_functions_resource_group"
  location = "West Europe"
}

resource "azurerm_storage_account" "scenario-sa" {
  name                     = "scenariosa"
  resource_group_name      = "${azurerm_resource_group.rg.name}"
  location                 = "${azurerm_resource_group.rg.location}"
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_app_service_plan" "scenario-service-plan" {
  name                = "azure-functions-service-plan"
  location            = "${azurerm_resource_group.rg.location}"
  resource_group_name = "${azurerm_resource_group.rg.name}"
  kind                = "FunctionApp"

  sku {
    tier = "Dynamic"
    size = "Y1"
  }
}

resource "azurerm_application_insights" "azure-functions-app-insights" {
  name                = "azure-functions-appinsights"
  location            = "${azurerm_resource_group.rg.location}"
  resource_group_name = "${azurerm_resource_group.rg.name}"
  application_type    = "Web"
}

module "scenario1" {
  source                    = "./scenario1"
  location                  = "${azurerm_resource_group.rg.location}"
  resource_group_name       = "${azurerm_resource_group.rg.name}"
  app_service_plan_id       = "${azurerm_app_service_plan.scenario-service-plan.id}"
  storage_connection_string = "${azurerm_storage_account.scenario-sa.primary_connection_string}"
  instrumentation_key       = "${azurerm_application_insights.azure-functions-app-insights.instrumentation_key}"
}
